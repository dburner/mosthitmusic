﻿/// <reference path="knockout-2.2.0.debug.js" />
/// <reference path="semantic.js" />
/// <reference path="jquery-1.8.2.intellisense.js" />

function TopMusicViewModel(serverModel, actionUrl, getSourceActionUrl)
{
    self = this;

    self.Songs        = ko.observableArray(serverModel.Songs);
    self.SnapshotId   = serverModel.SnapshotId;
    self.SnapshotTime = ko.observable(ParseServerTime(serverModel.SnapshotTime))
    self.ActionUrl    = actionUrl;
    self.LogoUrl      = ko.observable(serverModel.LogoUrl);
    self.SourceId     = serverModel.SourceId;
    self.GetSourceActionUrl = getSourceActionUrl;

    self.SnapshotFormatedTime = ko.computed(function () {
        var val = self.SnapshotTime();
        
        return val.toLocaleString();
    });

    function ParseServerTime(serverTime)
    {
        var value = new Date(parseInt(serverTime.substr(6)));

        return value;
    }

    this.PreviousSnapshot = function () {
        GetSnapshot(false);
    };

    this.NextSnapshot = function () {
        GetSnapshot(true);
    };

    this.PreviousSource = function () {
        GetSource(false);
    };

    this.NextSource = function () {
        GetSource(true);
    };

    this.afterAddAnimation = function (element, index, data) {
        if (element instanceof HTMLElement)
        {
            //$(element).hide().fadeIn(1000);
            //$(element).hide().transition("slide down");
        }
    };

    this.beforeRemoveAnimation = function (element, index, data) {
        if (element instanceof HTMLElement)
        {
            $(element).fadeOut(function () { $(element).remove(); })
            //$(element).transition("slide up", function () { $(element).remove(); });

        }
    };

    function GetSnapshot(getNext)
    {
        $.ajax(
            {
                url: self.ActionUrl,
                data:
                    {
                        snapshotId: self.SnapshotId,
                        getNext: getNext,
                        currentTime: self.SnapshotTime().toLocaleString(),
                    },
                success: function (eventArgs)
                {
                    self.Songs.removeAll();

                    self.SnapshotId = eventArgs.SnapshotId;

                    self.Songs(eventArgs.Songs);
                    self.LogoUrl(eventArgs.LogoUrl);
                    self.SnapshotTime(ParseServerTime(eventArgs.SnapshotTime));
                    

                    console.log(self.SnapshotId);
                },
            });
    }

    function GetSource(goNext)
    {
        $.ajax(
            {
                url: self.GetSourceActionUrl,
                data:
                    {
                        currentSource: self.SourceId,
                        goNext: goNext,
                    },

                success: function(eventArgs)
                {
                    self.LogoUrl(eventArgs.LogoUrl);
                    self.SourceId = eventArgs.SourceId;
                },
            });
    }
}
//var topMusicViewModel = new TopMusicViewModel(serverModel);
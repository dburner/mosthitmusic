﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Models;

namespace TopMusic.Web.ViewModels
{
    public class TopMusicViewModel
    {
        public IEnumerable<Song> Songs { get; set; }

        public Guid SnapshotId { get; set; }

        public Guid SourceId { get; set; }

        public string LogoUrl { get; set; }

        public DateTime SnapshotTime { get; set; }
    }
}
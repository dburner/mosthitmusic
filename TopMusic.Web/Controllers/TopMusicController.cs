﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using TopMusic.DAL;
using TopMusic.Web.Models;
using TopMusic.Web.ViewModels;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Utils;

namespace TopMusic.Web.Controllers
{
    public class TopMusicController : Controller
    {

        //
        // GET: /TopMusic/
        private IMusicContext db = new MusicContext();

        public TopMusicController()
        {
            db = new MusicContext()
            {
                Database = 
                {
                    Log = s => System.Diagnostics.Debug.WriteLine(s),
                },
            };
        }

        public TopMusicController(IMusicContext dbContext)
        {
            db = dbContext;
        }

        public ActionResult Index()
        {
            
            //db.Database.Log = 

            // .Include may be usefull
            SnapshotModel snapshot = db.Snapshots
                .Include(s => s.Source)
                .Include(s => s.Songs)
                .Include(s => s.Songs.Select(p => p.Song))
                .Include(s => s.Songs.Select(p => p.Song.Artist))
                .OrderByDescending(s => s.Time)
                .FirstOrDefault();

            IEnumerable<SongPositionModel> songs = snapshot.Songs.OrderBy( s=> s.Position);

            TopMusicViewModel vm = new TopMusicViewModel
            {
                Songs        = GetSimpleSongModel(songs),
                SnapshotId   = snapshot.SnapshotId,
                SnapshotTime = snapshot.Time,
                LogoUrl      = snapshot.Source.LogoUrl,
                SourceId     = snapshot.Source.TopId,
            };

            return View(vm);
        }

        public ActionResult GetNextSourceId(Guid currentSource, bool goNext)
        {
            TopSourceModel source = db.Sources
                .ToArray()
                .SkipWhile(s => s.TopId != currentSource)
                .Skip(1)
                .FirstOrDefault();

            if (source == null)
                source = db.Sources.FirstOrDefault();

            var result = new
            {
                LogoUrl = source.LogoUrl,
                SourceId = source.TopId,
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetNextSnapshot(Guid? snapshotId, bool getNext = false)
        {
            SnapshotModel snapshot = null;

            // TODO: move this into BLL
            if (getNext)
            {
                snapshot = db.Snapshots
                    .Include(s => s.Source)
                    .Include(s => s.Songs)
                    .Include(s => s.Songs.Select(p => p.Song))
                    .Include(s => s.Songs.Select(p => p.Song.Artist))
                    .OrderBy(s => s.Time)
                    .Where(s => s.Time > db.Snapshots.FirstOrDefault(p => p.SnapshotId == snapshotId).Time)
                    .FirstOrDefault();
            }
            else
            {
                snapshot = db.Snapshots
                    .Include(s => s.Source)
                    .Include(s => s.Songs)
                    .Include(s => s.Songs.Select(p => p.Song))
                    .Include(s => s.Songs.Select(p => p.Song.Artist))
                    .OrderByDescending(s => s.Time)
                    .Where(s => s.Time < db.Snapshots.FirstOrDefault(p => p.SnapshotId == snapshotId).Time)
                    .FirstOrDefault();
            }

            if (snapshot == null)
                return HttpNotFound();

            IEnumerable<SongPositionModel> songs = snapshot.Songs.OrderBy(s => s.Position).ToList();

            TopMusicViewModel vm = new TopMusicViewModel
            {
                Songs        = GetSimpleSongModel(songs),
                SnapshotId   = snapshot.SnapshotId,
                SnapshotTime = snapshot.Time,
                LogoUrl      = snapshot.Source.LogoUrl,
            };

            return Json(vm, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<Song> GetSimpleSongModel(IEnumerable<SongPositionModel> songs)
        {
            return songs.Select( s => new Song()
                {
                     SongName = s.Song.Name,
                     Artist   = s.Song.Artist.Name,
                     Position = s.Position,
                });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TopMusic.Web.Models
{
    public class SongModel
    {
        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid SongId { get; set; }

        public Guid ArtistId { get; set; }

        [ForeignKey("ArtistId")]
        public virtual ArtistModel Artist { get; set;}

        public string Name { get; set; }
    }

    public class ArtistModel
    {
        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid ArtistId { get; set; }

        public string Name { get; set; }
    }

    public class SongPositionModel
    {
        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid PositionId { get; set; }

        public int Position { get; set; }

        public Guid SongId     { get; set; }
        public Guid SnapshotId { get; set; }

        [ForeignKey("SongId")] 
        public virtual SongModel Song { get; set; }

        [ForeignKey("SnapshotId")]
        public virtual SnapshotModel Snapshot { get; set; }
    }

    public class SnapshotModel
    {
        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid SnapshotId { get; set; }

        [DataType(DataType.Date)]
        public DateTime Time { get; set; }

        public Guid SourceId { get; set; }

        [ForeignKey("SourceId")]
        public virtual TopSourceModel Source { get; set; }

        public virtual ICollection<SongPositionModel> Songs { get; set; }
    }

    public class TopSourceModel
    {
        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid TopId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string LogoUrl { get; set; }
    }
}
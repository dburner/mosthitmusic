﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopMusic.Models.Music.Simple
{
    public class Song
    {
        public int    Position { get; set; }
        public string Artist   { get; set; }
        public string SongName { get; set; }

        public override string ToString()
        {
            return string.Format("{0} \t - {1} - {2}", Position, Artist, SongName);
        }
    }
}

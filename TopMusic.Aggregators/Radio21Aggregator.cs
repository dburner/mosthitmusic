﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;
using TopMusic.Aggregators;
using TopMusic.Aggregators.DataSources;
using TopMusic.Models.Music.Simple;

namespace TopMusic.Web.Aggregators
{
    public class Radio21Aggregator : ISnapshotMaker
    {
        private const string c_Url = "http://www.radio21.ro/21-top-30/";
        private static char[] c_split = new char[] { ' ' };

        private static Guid SourceId = Guid.Parse("C86EC2E9-2514-4580-88B6-17921D0A28F1");

        private IMusicDataSource _dataSource;

        /// <summary>
        /// Normal constructor
        /// </summary>
        public Radio21Aggregator()
        {
            _dataSource = new WebDataSource(c_Url);
        }

        /// <summary>
        /// Constructor for Unit testing
        /// </summary>
        /// <param name="datasource"></param>
        public Radio21Aggregator(IMusicDataSource datasource)
        {
            _dataSource = datasource;
        }

        /// <summary>
        /// Takes a snapshot of a website top music
        /// </summary>
        /// <returns></returns>
        public Snapshot TakeSnapshot()
        {
            IEnumerable<Song> songsList = ParseSongsFromWeb();

            Snapshot snapshot = new Snapshot()
            {
                Songs  = songsList,
                Source = SourceId,
            };
            
            return snapshot;
        }

        /// <summary>
        /// Parses the songs from a web page
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Song> ParseSongsFromWeb()
        {
            HtmlDocument doc = _dataSource.GetData();

            var trNodes = doc.DocumentNode.SelectNodes("//*[@id=\"the-list\"]/div");

            List<Song> songsList = new List<Song>();

            foreach (HtmlNode trNode in trNodes)
            {
                Song song = GetModel(trNode);

                songsList.Add(song);
            }

            return songsList;
        }

        /// <summary>
        /// Gets the model from a HTML node
        /// </summary>
        /// <param name="trNode"></param>
        /// <returns>A song model</returns>
        private Song GetModel(HtmlNode trNode)
        {
            string pos = trNode.Descendants().Where(d =>
                d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("nrCrt")
            ).First().InnerText;
            string artist = trNode.Descendants().Where(d => 
                d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("artistN")
            ).First().InnerText;
            string songName     = trNode.Descendants().Where(d =>
                d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("songN")
            ).First().InnerText;

            string artistAndPos = pos + " " + artist;

            return GetModel(artistAndPos, songName);
        }

        /// <summary>
        /// Gets a song model from two strings
        /// </summary>
        /// <param name="artistAndPos">Artist and position string</param>
        /// <param name="songName">A string representing the song name</param>
        /// <returns>A Song model</returns>
        private Song GetModel(string artistAndPos, string songName)
        {
            string[] split = artistAndPos.Split(c_split, 2, StringSplitOptions.None);

            int pos = int.Parse(split[0]);

            Song song = new Song()
            {
                Position = pos,
                Artist   = split[1],
                SongName = songName,
            };

            return song;
        }
    }
}
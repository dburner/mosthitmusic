﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.Models.Music.Simple;

namespace TopMusic.Aggregators
{
    public interface ISnapshotMaker
    {
        Snapshot TakeSnapshot();
    }
}

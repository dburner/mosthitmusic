﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using System.Xml.XPath;
using TopMusic.Aggregators;
using TopMusic.Aggregators.DataSources;
using TopMusic.Models.Music.Simple;

namespace TopMusic.Web.Aggregators
{
    public class ProFmAggregator : ISnapshotMaker
    {
        private const string c_Url = "http://www.profm.ro/top/";
        private static char[] c_split = new char[] { ' ' };

        private static Guid SourceId = Guid.Parse("48081a6b-343a-49e0-aeca-f8d5d8f656f8");

        private IMusicDataSource _dataSource;

        /// <summary>
        /// Normal constructor
        /// </summary>
        public ProFmAggregator()
        {
            _dataSource = new WebDataSource(c_Url);
        }

        /// <summary>
        /// Constructor for Unit testing
        /// </summary>
        /// <param name="datasource"></param>
        public ProFmAggregator(IMusicDataSource datasource)
        {
            _dataSource = datasource;
        }

        /// <summary>
        /// Takes a snapshot of a website top music
        /// </summary>
        /// <returns></returns>
        public Snapshot TakeSnapshot()
        {
            IEnumerable<Song> songsList = ParseSongsFromWeb();

            Snapshot snapshot = new Snapshot()
            {
                Songs = songsList,
                Source = SourceId,
            };

            return snapshot;
        }

        /// <summary>
        /// Parses the songs from a web page
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Song> ParseSongsFromWeb()
        {
            HtmlDocument doc = _dataSource.GetData();

            var nodes = doc.DocumentNode.SelectNodes("//*[@class='widget category-view top-widget-page']/div");
            nodes.Remove(nodes.First());
            List<Song> songsList = new List<Song>();

            foreach (HtmlNode trNode in nodes)
            {
                Song song = GetModel(trNode);

                songsList.Add(song);
            }

            return songsList;
        }

        /// <summary>
        /// Gets the model from a HTML node
        /// </summary>
        /// <param name="node"></param>
        /// <returns>A song model</returns>
        private Song GetModel(HtmlNode node)
        {
            string pos    = node.SelectSingleNode("./*[@class='evolution']")
                                   .SelectSingleNode("./*[@class='order']")
                                   .InnerText;
            string track  = node.SelectSingleNode("./*[@class='song']")
                                   .SelectSingleNode("./*[@class='song-title']")
                                   .InnerText.Trim();
            string artist = node.SelectSingleNode("./*[@class='song']")
                                   .SelectSingleNode("./*[@class='artist']")
                                   .InnerText.Trim();

            string artistAndPos = pos + " " + artist;

            return GetModel(artistAndPos, track);
        }

        /// <summary>
        /// Gets a song model from two strings
        /// </summary>
        /// <param name="artistAndPos">Artist and position string</param>
        /// <param name="songName">A string representing the song name</param>
        /// <returns>A Song model</returns>
        private Song GetModel(string artistAndPos, string songName)
        {
            string[] split = artistAndPos.Split(c_split, 2, StringSplitOptions.None);

            int pos = int.Parse(split[0]);

            Song song = new Song()
            {
                Position = pos,
                Artist = split[1],
                SongName = songName,
            };

            return song;
        }
    }
}

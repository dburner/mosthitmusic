﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.Aggregators.DataSources;
using TopMusic.Models.Music.Simple;
using Newtonsoft.Json;

namespace TopMusic.Aggregators
{
    public class KissFmAggregator : ISnapshotMaker
    {
        #region Json Models
        // Generated using http://jsonutils.com/
        // Some classes were removed because they were not needed

        public class VideoId
        {

            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("titlu")]
            public string Titlu { get; set; }
        }

        public class KissFmSong
        {

            [JsonProperty("id")]
            public int Id { get; set; }

            [JsonProperty("pozitie")]
            public int Pozitie { get; set; }

            [JsonProperty("pozitie_anterioara")]
            public int PozitieAnterioara { get; set; }

            [JsonProperty("piesa")]
            public string Piesa { get; set; }

            [JsonProperty("artist")]
            public string Artist { get; set; }

            [JsonProperty("pic")]
            public string Pic { get; set; }

            [JsonProperty("video_id")]
            public VideoId VideoId { get; set; }

            [JsonProperty("diferenta")]
            public int Diferenta { get; set; }

            [JsonProperty("top")]
            public string Top { get; set; }
        }

        public class KissFmApiData
        {

            [JsonProperty("data_inceput")]
            public string DataInceput { get; set; }

            [JsonProperty("data_sfarsit")]
            public string DataSfarsit { get; set; }

            [JsonProperty("items")]
            public IList<KissFmSong> Items { get; set; }

            [JsonProperty("error_code")]
            public int ErrorCode { get; set; }

            [JsonProperty("error_message")]
            public object ErrorMessage { get; set; }
        }

        #endregion


        private const string c_Url = "https://api.kissfm.ro/api/kisstop40";
        private static char[] c_split = new char[] { ' ' };

        private static Guid SourceId = Guid.Parse("EA4B832A-6C0E-4030-89E7-56598D331F8B");

        private IJsonMusicDataSource _dataSource;

        /// <summary>
        /// Normal constructor
        /// </summary>
        public KissFmAggregator()
        {
            _dataSource = new WebDataSource(c_Url);
        }

        /// <summary>
        /// Constructor for Unit testing
        /// </summary>
        /// <param name="datasource"></param>
        public KissFmAggregator(IJsonMusicDataSource datasource)
        {
            _dataSource = datasource;
        }

        public Snapshot TakeSnapshot()
        {
            IEnumerable<Song> songsList = ParseSongsFromWeb();

            Snapshot snapshot = new Snapshot()
            {
                Songs = songsList,
                Source = SourceId,
            };

            return snapshot;
        }

        private IEnumerable<Song> ParseSongsFromWeb()
        {
            string doc = _dataSource.GetJsonData();

            var json = JsonConvert.DeserializeObject<KissFmApiData>(doc);

            var songsList = json.Items.Select(s => GetModel(s)).ToArray();

            return songsList;
        }

        private Song GetModel(KissFmSong jsonSong)
        {           
            Song song = new Song()
            {
                Artist   = jsonSong.Artist,
                SongName = jsonSong.Piesa,
                Position = jsonSong.Pozitie,
            };

            return song;
        }
    }
}

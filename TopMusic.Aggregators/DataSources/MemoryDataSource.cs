﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopMusic.Aggregators.DataSources
{
    /// <summary>
    /// Loads data from memory not from a web source
    /// </summary>
    public class MemoryDataSource : IMusicDataSource, IJsonMusicDataSource
    {
        private string _html;

        public MemoryDataSource(string html)
        {
            _html = html;
        }

        public HtmlDocument GetData()
        {
            HtmlDocument document = new HtmlDocument();

            document.LoadHtml(_html);

            return document;
        }

        public string GetJsonData()
        {
            return _html;
        }
    }
}

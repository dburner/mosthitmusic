﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopMusic.Aggregators.DataSources
{
    public interface IJsonMusicDataSource
    {
        string GetJsonData();
    }
}

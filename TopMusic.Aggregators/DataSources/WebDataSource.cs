﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace TopMusic.Aggregators.DataSources
{
    internal class WebDataSource : IMusicDataSource, IJsonMusicDataSource
    {
        private string _dataUrl;

        public WebDataSource(string dataUrl)
        {
            _dataUrl = dataUrl;
        }

        public HtmlDocument GetData()
        {
            HtmlWeb web = new HtmlWeb();

            return web.Load(_dataUrl);
        }

        public string GetJsonData()
        {
            WebClient web = new WebClient();

            return web.DownloadString(_dataUrl);
        }
    }
}

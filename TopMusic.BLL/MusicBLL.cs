﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.DAL;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Models;

namespace TopMusic.BLL
{
    public class MusicBLL : IDisposable
    {
        private IMusicContext db;

        public MusicBLL()
        {
            db = new MusicContext();
        }

        public MusicBLL(IMusicContext context)
        {
            db = context;
        }

        public void ProcessSnapshots(IEnumerable<Snapshot> snapshots)
        {
            foreach(Snapshot snapshot in snapshots)
            {
                ProcessSnapshotInternal(snapshot);
            }

            db.SaveChanges();
        }

        public void ProcessSnapshot(Snapshot snapshot)
        {
            ProcessSnapshotInternal(snapshot);

            db.SaveChanges();
        }

        private void ProcessSnapshotInternal(Snapshot snapshot)
        {
            var snapshotModel = AddSnapshotToContext(snapshot.Source);

            foreach (var song in snapshot.Songs)
            {
                // PERFORMANCE: critical this makes 2 requests foreach song
                var artist    = db.Artists.FirstOrDefault(s => s.Name == song.Artist);
                var songModel = db.Songs.FirstOrDefault(s => s.Name == song.SongName);

                if (artist == null)
                    artist = AddArtistToContext(song.Artist);

                if (songModel == null)
                    songModel = AddSongToContext(song.SongName, artist.ArtistId);

                db.SongsPosition.Add(new SongPositionModel()
                {
                    SongId     = songModel.SongId,
                    Position   = song.Position,
                    PositionId = Guid.NewGuid(),
                    SnapshotId = snapshotModel.SnapshotId,
                });
            }
        }

        private ArtistModel AddArtistToContext(string name)
        {
            ArtistModel artist = new ArtistModel
            {
                ArtistId = Guid.NewGuid(),
                Name = name,
            };

            db.Artists.Add(artist);

            return artist;
        }

        private SongModel AddSongToContext(string songName, Guid artistId)
        {
            SongModel model = new SongModel()
            {
                ArtistId = artistId,
                SongId = Guid.NewGuid(),
                Name = songName,
            };

            db.Songs.Add(model);

            return model;
        }

        private SnapshotModel AddSnapshotToContext(Guid sourceId)
        {
            SnapshotModel model = new SnapshotModel()
            {
                SnapshotId = Guid.NewGuid(),
                Time       = DateTime.Now,
                SourceId   = sourceId,
            };

            db.Snapshots.Add(model);

            return model;
        }


        public void Dispose()
        {
            db.Dispose();
        }

        // TODO: Implement corect disposable pattern
        ~MusicBLL()
        {
            Dispose();
        }
    }
}

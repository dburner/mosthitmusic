﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.Web.Models;

namespace TopMusic.DAL
{
    public class MusicContext : DbContext, IMusicContext
    {
        public MusicContext()
            : base("name=DefaultConnection")
        {
            Database.SetInitializer<MusicContext>(new MyContextInitializer());
            this.Database.Initialize(true);
        }

        public DbSet<SongModel>         Songs         { get; set; }
        public DbSet<SongPositionModel> SongsPosition { get; set; }
        public DbSet<ArtistModel>       Artists       { get; set; }
        public DbSet<TopSourceModel>    Sources       { get; set; }
        public DbSet<SnapshotModel>     Snapshots     { get; set; }

        public void SetEntryState<TEntity>(TEntity entry, EntityState state) where TEntity : class
        {
            Entry(entry).State = state;
        }
    }

    public class MyContextInitializer :  CreateDatabaseIfNotExists<MusicContext>
    {
        protected override void Seed(MusicContext context)
        {
            Guid radio21Id = Guid.Parse("C86EC2E9-2514-4580-88B6-17921D0A28F1");
            Guid kissFmId  = Guid.Parse("EA4B832A-6C0E-4030-89E7-56598D331F8B");
            Guid proFmId   = Guid.Parse("48081a6b-343a-49e0-aeca-f8d5d8f656f8");

            context.Sources.Add(new TopSourceModel() { Name = "Radio21", TopId = radio21Id });
            context.Sources.Add(new TopSourceModel() { Name = "KissFm",  TopId = kissFmId  });
            context.Sources.Add(new TopSourceModel() { Name = "ProFM",   TopId = proFmId   });


            //EF will call SaveChanges itself
        }
    }
}

﻿using System;
using System.Data.Entity;
using TopMusic.Web.Models;
namespace TopMusic.DAL
{
    public interface IMusicContext : IDisposable
    {
        DbSet<ArtistModel>       Artists       { get; set; }
        DbSet<SongModel>         Songs         { get; set; }
        DbSet<SongPositionModel> SongsPosition { get; set; }
        DbSet<TopSourceModel>    Sources       { get; set; }
        DbSet<SnapshotModel>     Snapshots     { get; set; }

        int SaveChanges();

        void SetEntryState<TEntity>(TEntity entry, EntityState state) where TEntity : class;
    }
}

namespace TopMusic.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ArtistModels",
                c => new
                    {
                        ArtistId = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ArtistId);
            
            CreateTable(
                "dbo.SnapshotModels",
                c => new
                    {
                        SnapshotId = c.Guid(nullable: false),
                        Time = c.DateTime(nullable: false),
                        SourceId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.SnapshotId)
                .ForeignKey("dbo.TopSourceModels", t => t.SourceId, cascadeDelete: true)
                .Index(t => t.SourceId);
            
            CreateTable(
                "dbo.SongPositionModels",
                c => new
                    {
                        PositionId = c.Guid(nullable: false),
                        Position = c.Int(nullable: false),
                        SongId = c.Guid(nullable: false),
                        SnapshotId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.PositionId)
                .ForeignKey("dbo.SnapshotModels", t => t.SnapshotId, cascadeDelete: true)
                .ForeignKey("dbo.SongModels", t => t.SongId, cascadeDelete: true)
                .Index(t => t.SongId)
                .Index(t => t.SnapshotId);
            
            CreateTable(
                "dbo.SongModels",
                c => new
                    {
                        SongId = c.Guid(nullable: false),
                        ArtistId = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SongId)
                .ForeignKey("dbo.ArtistModels", t => t.ArtistId, cascadeDelete: true)
                .Index(t => t.ArtistId);
            
            CreateTable(
                "dbo.TopSourceModels",
                c => new
                    {
                        TopId = c.Guid(nullable: false),
                        Name = c.String(),
                        Url = c.String(),
                    })
                .PrimaryKey(t => t.TopId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SnapshotModels", "SourceId", "dbo.TopSourceModels");
            DropForeignKey("dbo.SongPositionModels", "SongId", "dbo.SongModels");
            DropForeignKey("dbo.SongModels", "ArtistId", "dbo.ArtistModels");
            DropForeignKey("dbo.SongPositionModels", "SnapshotId", "dbo.SnapshotModels");
            DropIndex("dbo.SongModels", new[] { "ArtistId" });
            DropIndex("dbo.SongPositionModels", new[] { "SnapshotId" });
            DropIndex("dbo.SongPositionModels", new[] { "SongId" });
            DropIndex("dbo.SnapshotModels", new[] { "SourceId" });
            DropTable("dbo.TopSourceModels");
            DropTable("dbo.SongModels");
            DropTable("dbo.SongPositionModels");
            DropTable("dbo.SnapshotModels");
            DropTable("dbo.ArtistModels");
        }
    }
}

namespace TopMusic.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LogoUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TopSourceModels", "LogoUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TopSourceModels", "LogoUrl");
        }
    }
}

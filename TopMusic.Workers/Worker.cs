﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.Aggregators;
using TopMusic.BLL;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Aggregators;
using TopMusic.Web.Models;

namespace TopMusic.Workers
{
    public class Worker
    {
        private MusicBLL musicBLL = new MusicBLL();

        private IEnumerable<ISnapshotMaker> _aggregators = new ISnapshotMaker[]
        {
            new Radio21Aggregator(),
            //new ProFmAggregator(),
            //new KissFmAggregator()
        };

        public void DoWork()
        {
            List<Snapshot> snapshotList = new List<Snapshot>();

            foreach(ISnapshotMaker aggregator in _aggregators )
            {
                var res = aggregator.TakeSnapshot();

                snapshotList.Add(res);
            }

            musicBLL.ProcessSnapshots(snapshotList);
        }
    }
}

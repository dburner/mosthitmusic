﻿using System;
using NUnit.Framework;
using TopMusic.Workers;

namespace TopMusic.Web.Tests.Workers
{
    [TestFixture]
    public class WorkerTests
    {
        [Test, Explicit]
        public void MakeSnapshot()
        {
            Worker worker = new Worker();

            worker.DoWork();
        }
    }
}

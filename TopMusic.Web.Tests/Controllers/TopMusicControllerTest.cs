﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.Web.Controllers;
using TopMusic.Web.Models;
using TopMusic.Web.Tests.FakeDal;

namespace TopMusic.Web.Tests.Controllers
{
    [TestFixture]
    public class TopMusicControllerTest
    {
        [Test]
        public void GetNextSnapshot()
        {
            int snapshotsCount = 5;
            FakeMusicContext context = new FakeMusicContext();

            var snapshots = GenerateSnapshots(snapshotsCount);

            context.Snapshots.AddRange(snapshots);

            TopMusicController controller = new TopMusicController(context);

            var result = controller.GetNextSnapshot(snapshots.Last().SnapshotId, false);
        }

        [Test]
        public void GetNextSourceId()
        {
            //FakeMusicContext context = new FakeMusicContext();

            TopMusicController controller = new TopMusicController();

            controller.GetNextSourceId(Guid.Parse("C86EC2E9-2514-4580-88B6-17921D0A28F1"), true);
        }

        private List<SnapshotModel> GenerateSnapshots(int count)
        {
            List<SnapshotModel> snapshotsList = new List<SnapshotModel>();

            TopSourceModel source = new TopSourceModel() { LogoUrl = "someurl", Name = "SorceName", Url = "someUrl2" };

            DateTime time = new DateTime(2000, 1, 1);

            for(int i = 0; i < count; i++)
            {
                SnapshotModel model = new SnapshotModel()
                {
                    SnapshotId = Guid.NewGuid(),
                    Time  = time.AddDays(i),
                    Songs = GenerateSongModels(5),
                    Source = source,
                };

                snapshotsList.Add(model);
            }

            return snapshotsList;
        }

        private List<SongPositionModel> GenerateSongModels(int count)
        {
            List<SongPositionModel> songsList = new List<SongPositionModel>();

            for (int i = 0; i < count; i++)
            {
                SongPositionModel model = new SongPositionModel()
                {
                    Position = i,
                    Song = new SongModel()
                    {
                        Name = "Song" + i,
                        Artist = new ArtistModel()
                        {
                            Name = "Artist" + i,
                        },
                    }
                };

                songsList.Add(model);
            }

            return songsList;
        }
    }
}

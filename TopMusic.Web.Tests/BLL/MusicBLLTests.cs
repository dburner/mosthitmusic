﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.BLL;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Tests.FakeDal;

namespace TopMusic.Web.Tests.BLL
{
    [TestFixture]
    public class MusicBLLTests
    {
        [Test]
        public void ProcessSnapshotsTest()
        {
            FakeMusicContext context = new FakeMusicContext();

            MusicBLL bll = new MusicBLL(context);

            IEnumerable<Snapshot> snapshots = GetSnapshots();

            bll.ProcessSnapshots(snapshots);

            Assert.That(context.Artists.Local.Count,   Is.EqualTo(2));
            Assert.That(context.Snapshots.Local.Count, Is.EqualTo(1));
        }

        private IEnumerable<Snapshot> GetSnapshots()
        {
            var snapshots = new[]
            {
                new Snapshot()
                {
                     Source = Guid.Empty,

                     Songs = new[]
                     {
                         new Song()
                         {
                              Artist = "Margineanu",
                              Position = 1,
                              SongName = "Gore",
                         },
                         new Song()
                         {
                              Artist = "SubFocus",
                              Position = 2,
                              SongName = "Rock It",
                         },
                     }
                }
            };

            return snapshots;
        }
    }
}

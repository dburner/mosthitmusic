﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TopMusic.Aggregators.DataSources;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Aggregators;
using TopMusic.Web.Tests.Properties;

namespace TopMusic.Web.Tests.Aggregators
{
    [TestFixture]
    public class Radio21Tests
    {
        [Test]
        public void Radio21_From_Web()
        {
            const int radio21TopCount = 30;

            Radio21Aggregator agregator = new Radio21Aggregator();

            Snapshot result = agregator.TakeSnapshot();

            Assert.That(result.Songs.Count(), Is.EqualTo(radio21TopCount));
        }

        [Test]
        public void Radio21_From_Web_Data_Check()
        {
            Radio21Aggregator agregator = new Radio21Aggregator();

            Snapshot result = agregator.TakeSnapshot();

            Song song = result.Songs.First();
            
            Assert.That(song.Artist.GetType(), Is.EqualTo(typeof(string)));
            Assert.That(song.Position.GetType(), Is.EqualTo(typeof(int)));
            Assert.That(song.SongName.GetType(), Is.EqualTo(typeof(string)));
        }

        [Test]
        public void Radio21_From_Memory()
        {
            const int radio21TopCount = 50;

            MemoryDataSource dataSource = new MemoryDataSource(Resources.Radio21Html);

            Radio21Aggregator agregator = new Radio21Aggregator();

            Snapshot result = agregator.TakeSnapshot();

            Assert.That(result.Songs.Count(), Is.EqualTo(radio21TopCount));
        }
    }
}

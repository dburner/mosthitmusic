﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TopMusic.Aggregators;
using TopMusic.Aggregators.DataSources;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Tests.Properties;

namespace TopMusic.Web.Tests.Aggregators
{
    [TestFixture]
    public class KissFmTests
    {
        private const int songsCount = 40;

        #region Expected values

        private IEnumerable<string> _expectedSongNames = new string[]
        {
            "SUB PIELEA MEA",
            "DON'T BE SO SHY",
            "IUBIREA SCHIMBA TOT",
            "Ego",
            "GURA TA",
            "CASTLE IN THE SNOW",
            "Be The One",
            "Ma ucide ea",
            "Ocean Drive",
            "Bonbon",
            "Dansam",
            "YALLA",
            "Te Rog",
            "Rush",
            "AMOR",
            "In oglinda",
            "GET UGLY",
            "PIERDUT BULETIN",
            "I DID IT MAMA!",
            "Mesajul meu",
            "Hotline Bling",
            "Neprevazut",
            "NEVER FORGET YOU",
            "Can't Feel My Face",
            "REALITY",
            "Ne impotrivim",
            "LUPII",
            "Adventure Of A Lifetime",
            "Sugar",
            "Make Up",
            "FADED",
            "LE-AM SPUS SI FETELOR",
            "LIGHT IT UP",
            "Runnin'",
            "DULCE AMAR",
            "LOCKED AWAY",
            "Pillow Talk",
            "Love Yourself (PURPOSE : The Movement) ",
            "CREIOANE COLORATE",
            "Secret Love Song",
        };

        private IEnumerable<string> _expectedArtistNames = new string[]
        {
            "CARLA'S DREAMS",
            "IMANY",
            "ANDRA",
            "Willy William",
            "DELIA MATACHE FEAT. DEEPCENTRAL",
            "KADEBOSTANY",
            "Dua Lipa",
            "Mihail",
            "Duke Dumont",
            "Era Istrefi",
            "Randi",
            "INNA",
            "Carla's Dream",
            "Zwette feat. Molly",
            "KAMELIA",
            "Amna feat. Robert Toma ",
            "JASON DERULO",
            "DOC & MOTZU FEAT. SMILEY",
            "ALEXANDRA STAN",
            "Jo",
            "Drake",
            "Corina",
            "MNEK FEAT. ZARA LARSSON",
            "The Weeknd",
            "LOST FREQUENCIES FEAT. JANIECK DEVY",
            "Lora",
            "ADDA",
            " Coldplay",
            "Robin Schulz",
            "R City & Chloe Angelides",
            "ALAN WALKER",
            "LIDIA BUBLE FEAT. AMIRA",
            "MAJOR LAZER FEAT. NYLA",
            "Naughty Boy & Beyonce",
            "DJ SAVA FEAT. ALINA EREMIA, WHAT'S UP",
            "R. CITY FEAT. ADAM LEVINE",
            "Zayn",
            "Justin Bieber",
            "FELI",
            "Little Mix ft. Jason Derulo",
        };

        #endregion

        [Test]
        public void KissFm_From_Memory()
        {
            // Arrange
            MemoryDataSource dataSource = new MemoryDataSource(Resources.KissFmJson);

            KissFmAggregator agregator = new KissFmAggregator(dataSource);

            // Act
            Snapshot result = agregator.TakeSnapshot();

            // Assert
            Assert.That(result.Songs.Count(), Is.EqualTo(songsCount));

            var songNames   = result.Songs.Select(s => s.SongName).ToArray();
            var artistNames = result.Songs.Select(s => s.Artist  ).ToArray();

            CollectionAssert.AreEqual(_expectedSongNames,   songNames,     "Song names are not propperly decoded");
            CollectionAssert.AreEqual(_expectedArtistNames, artistNames, "Artist names are not propperly decoded");
        }

        [Test]
        public void KissFm_From_Web()
        {
            KissFmAggregator agregator = new KissFmAggregator();

            Snapshot result = agregator.TakeSnapshot();

            Assert.That(result.Songs.Count(), Is.EqualTo(songsCount));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TopMusic.Aggregators.DataSources;
using TopMusic.Models.Music.Simple;
using TopMusic.Web.Aggregators;
using TopMusic.Web.Tests.Properties;

namespace TopMusic.Web.Tests.Aggregators
{
    [TestFixture]
    public class ProFmTests
    {
        [Test]
        public void ProFm_From_Web_Data_Check()
        {
            ProFmAggregator agregator = new ProFmAggregator();

            Snapshot result = agregator.TakeSnapshot();

            //Song song = result.Songs.First();

            foreach (Song song in result.Songs)
            {
                Assert.That(song.Artist.Length, Is.GreaterThan(0));
                Assert.That(song.SongName.Length, Is.GreaterThan(0));

                Assert.That(song.Position, Is.GreaterThan(0));
                Assert.That(song.Position, Is.LessThan(31));
            }
        }

    }
}

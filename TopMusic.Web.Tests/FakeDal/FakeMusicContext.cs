﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using TopMusic.DAL;
using TopMusic.Models;
using TopMusic.Web.Models;

namespace TopMusic.Web.Tests.FakeDal
{
    public class FakeMusicContext : IMusicContext
    {
        public FakeMusicContext()
        {
            Songs         = new FakeDbSet<SongModel>        ();
            SongsPosition = new FakeDbSet<SongPositionModel>();
            Artists       = new FakeDbSet<ArtistModel>      ();
            Sources       = new FakeDbSet<TopSourceModel>   ();
            Snapshots     = new FakeDbSet<SnapshotModel>    ();
        }

        public DbSet<SongModel>         Songs         { get; set; }
        public DbSet<SongPositionModel> SongsPosition { get; set; }
        public DbSet<ArtistModel>       Artists       { get; set; }
        public DbSet<TopSourceModel>    Sources       { get; set; }
        public DbSet<SnapshotModel>     Snapshots     { get; set; }

        public void SetEntryState<TEntity>(TEntity entry, EntityState state) where TEntity : class
        {
            //Entry(entry).State = state;
        }


        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose()
        {
            
        }
    }
}
